# 後藤 (a.k.a. @antimon2) のパブリックリポジトリ #

自習・研究等で作った諸々（公開できるもの）を置いておく場所。

### TOC ###

* Tutorials : 機械学習研修で作ったコードの置き場。
    * [AdaBoostSample.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/antimon2k/public/raw/5598f6e9e35f7f84134ce7515a038ac98e3ffbd0/Tutorials/AdaBoostSample.ipynb) : AdaBoost サンプル（リンク先は nbviewer による変換結果）
    * [RandomForest.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/antimon2k/public/raw/4844469e98dbc7564f98f2fb9e8ac9ac9eeb44eb/Tutorials/RandomForest.ipynb) : RandomForest サンプル（リンク先は nbviewer による変換結果）
    * [TensorFlowSample.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/antimon2k/public/raw/5e20998f92adff8e486f6479fed5fb2120293bf8/Tutorials/TensorFlowSample.ipynb) : NN with TensorFlow サンプル（リンク先は nbviewer による変換結果）